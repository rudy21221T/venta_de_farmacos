<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteM extends Model
{
	public $timestamps = false;
	protected $table = 'clientes';
	protected $fillable=['id_cliente','nombre_compania','nombre_contacto','telefono','id_ciudad'];
}
