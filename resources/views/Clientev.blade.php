
<body style="width: 100%; font-size: 100%;">
	<div class="container">
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" style="margin-top: 10px;">
			Ingresar Cliente
		</button>
		<table border="1" class="table table-dark table-hover" style=" width: 100%;">
			<thead>
				<tr>
					<td>N°</td>
					<td>Nombre de compañia</td>
					<td>Nombre de contacto</td>
					<td>Telefono</td>
					<td>Direccion</td>
					<td>Ciudad</td>
				</tr>
			</thead>
			<tbody>
				<?php $n = 1; foreach ($c as $v) { ?>
					<tr>
						<td><?php echo $n; ?></td>
						<td><?php echo $v->nombre_compania; ?></td>
						<td><?php echo $v->nombre_contacto; ?></td>
						<td><?php echo $v->direccion; ?></td>
						<td><?php echo $v->telefono; ?></td>
						<td><?php echo $v->id_ciudad; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>


		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Nuevo Cliente</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="" method="">
							<div>
								<label>Nombre de Compañia</label>
								<input type="text" name="compania" class="form-control" placeholder="Ingrese el nombre de comapñia...">
							</div>
							<div>
								<label>Nombre de Contacto</label>
								<input type="text" name="contacto" class="form-control" placeholder="Ingrese el nombre de contacto...">
							</div>
							<div>
								<label>Telefono</label>
								<input type="text" name="telefono" class="form-control" placeholder="Ingrese el telefono...">
							</div>
							<div>
								<label>Direccion</label>
								<input type="text" name="direccion" class="form-control" placeholder="Ingrese la direccion...">
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
							<input type="submit" name="Guardar" value="Guardar" class="btn btn-primary">
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>