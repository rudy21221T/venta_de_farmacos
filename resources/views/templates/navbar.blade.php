<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand"  style="color: white">Bienvenido/</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link"  style="color: white" href="#"><i class="fas fa-address-book"></i>Cliente<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link"  style="color: white" href="#">Factura</a>
      </li>
      <li class="nav-item">
        <a class="nav-link"  style="color: white" href="#">Producto</a>
      </li>
      <li class="nav-item">
        <a class="nav-link"  style="color: white" href="#"><i class="far fa-user"></i>Empleados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link"  style="color: white" href="#">Proveedores</a>
      </li>
    </ul>
  </div>
</nav>
